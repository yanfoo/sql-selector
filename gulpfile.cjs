const gulp = require("gulp");
const babel = require("gulp-babel");

const babelConfig = require('./babel.config.cjs');


gulp.task("default", function () {
  return gulp.src("src/**/*.js")
    .pipe(babel(babelConfig))
    .pipe(gulp.dest("lib"));
});
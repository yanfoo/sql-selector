import mySqlParser from '../../src/dialects/mysql';



describe('Testing MySQL dialect', () => {

   it('should return correct plaecholder', () => {
      expect( mySqlParser.getPlaceholder() ).toBe('?');
   });

   it('should parse query', () => {
      const params = [];
      const selector = {
         foo: 123
      };

      const sql = mySqlParser.parse(selector, params);

      expect( sql ).toBe('foo = ?');
      expect( params ).toEqual([ 123 ]);
   });

})
import standardHelpers from '../src/helpers';



describe('Testing standard helpers', () => {

   describe('$cast', () => {

      it('should wrap value', () => {
         expect( standardHelpers.$cast('foo', 'x', 'test') ).toEqual([ 'foo', 'CAST(x AS test)' ]);
      });

      it('should not wrap value', () => {
         expect( standardHelpers.$cast('foo', 'x', false) ).toEqual([ 'foo', 'x' ]);
      });

   });



});
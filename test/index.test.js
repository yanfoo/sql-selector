import * as SqlSelectorModule from '../src/index';



describe('Testing module entry point', () => {

   it('should expose everything', () => {

      // 'src/parser.js'
      expect( SqlSelectorModule ).toHaveProperty('DefaultParser');

      // 'src/operators'
      expect( SqlSelectorModule ).toHaveProperty('standardOperators');

      // 'src/parser-utils'
      expect( SqlSelectorModule ).toHaveProperty('parseSelector');
      expect( SqlSelectorModule ).toHaveProperty('processHelpers');
      expect( SqlSelectorModule ).toHaveProperty('checkArray');
      expect( SqlSelectorModule ).toHaveProperty('isBoolean');
      expect( SqlSelectorModule ).toHaveProperty('isNull');
      expect( SqlSelectorModule ).toHaveProperty('isPrimitive');
      expect( SqlSelectorModule ).toHaveProperty('isRegExp');

      // 'src/dialects/mysql.js'
      expect( SqlSelectorModule ).toHaveProperty('mySqlParser');

   });

});
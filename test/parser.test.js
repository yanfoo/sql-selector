import DefaultParser from '../src/parser';
import { processHelpers } from '../src/parser-utils';


describe('Testing abstract parser', () => {


   const testParse = (parser, selector, expectedExpr, expectedParams) => {
      const params = expectedParams ? [] : undefined;
      const where = parser.parse(selector, params);
      expect( where ).toBe(expectedExpr);
      expect( params ).toEqual( expectedParams);
   };

   
   it('should return empty if no key', () => {
      const parser = new DefaultParser();

      [
         undefined, null,
         {},
         [],
         [0], [0, 1],
         [''], ['foo'], ['foo', 'bar'],
         0, 1, 2,
         '', 'test'
      ].forEach(selector => testParse(parser, selector, ''));
   });


   describe('Do NOT use params', () => {

      const parser = new DefaultParser();
      
      // Logical

      describe('$eq / $ne', () => {

         it('should default to $q', () => {
            const d = new Date();

            testParse(parser, { a: 'Hello' },'a = "Hello"');
            testParse(parser, { a: 123 },'a = 123');
            testParse(parser, { a: null },'a IS NULL');
            testParse(parser, { a: d },'a = "' + d.toISOString() + '"');
            testParse(parser, { a: true },'a = TRUE');
            testParse(parser, { a: false },'a = FALSE');
            testParse(parser, { a: 'Hello', b: 123, c: true, d: null },'a = "Hello" AND b = 123 AND c = TRUE AND d IS NULL');
            testParse(parser, { a: { b: { c: 'foo' }, d: 123 }},'a.b.c = "foo" AND a.d = 123');
         });

         it('should equal', () => {
            testParse(parser, { a: { $eq: 'Hello' } },'a = "Hello"');
            testParse(parser, { a: { $eq: 123 } },'a = 123');
         });

         it('should equal null', () => {
            [
               null, undefined
            ].forEach(value => testParse(parser, { a: { $eq: value } }, 'a IS NULL'));
         });

         it('should not equal', () => {
            testParse(parser, { a: { $ne: 'Hello' } }, 'a <> "Hello"');
            testParse(parser, { a: { $ne: 123 } }, 'a <> 123');
         });

         it('should not equal null', () => {
            [
               null, undefined
            ].forEach(value => testParse(parser, { a: { $ne: value } }, 'a IS NOT NULL'));
         });

      });


      describe('$gt / $gte', () => {

         it('should be greater than', () => {
            testParse(parser, { a: { $gt: 123 } }, 'a > 123');
         });

         it('should be greater than or equal', () => {
            testParse(parser, { a: { $gte: 123 } }, 'a >= 123');
         });

      });



      describe('$lt / $lte', () => {

         it('should be less than', () => {
            testParse(parser, { a: { $lt: 123 } }, 'a < 123');
         });

         it('should be less than or equal', () => {
            testParse(parser, { a: { $lte: 123 } }, 'a <= 123');
         });

      });


      describe('$in / $nin', () => {

         it('should be in', () => {
            testParse(parser, { a: { $in: [ 123 ] } }, 'a IN (123)');
            testParse(parser, { a: { $in: [ 123, 456 ] } }, 'a IN (123,456)');
         });

         it('should not be in', () => {
            testParse(parser, { a: { $nin: [ 123 ] } }, 'a NOT IN (123)');
            testParse(parser, { a: { $nin: [ 123, 456 ] } }, 'a NOT IN (123,456)');
         });

         it('should throw if not an array', () => {
            expect(() => testParse(parser, { a: { $in: 123 } })).toThrow();
            expect(() => testParse(parser, { a: { $nin: 123 } })).toThrow();
         });

      });


      describe('$like', () => {

         it('should like', () => {
            testParse(parser, { a: { $like: '%test%' } }, 'a LIKE "%test%"');
         });

         it('should negate', () => {
            testParse(parser, { a: { $like: '%test%', $negate: true } }, 'a NOT LIKE "%test%"');
         });

      });


      describe('$regex', () => {

         it('should use default if using regular expression value', () => {
            testParse(parser, { a: /./ }, 'a REGEXP "."');
         });

         it('should use default if using regular expression value', () => {
            testParse(parser, { a: { $ne: /./ } }, 'a NOT REGEXP "."');
         });

         it('should use case insensitive', () => {
            testParse(parser, { a: /.ABC/i }, 'LOWER(a) REGEXP ".abc"');
            testParse(parser, { a: { $regex: /.ABC/i } }, 'LOWER(a) REGEXP ".abc"');
            testParse(parser, { a: { $regex: /.ABC/i, $options: 'i' } }, 'LOWER(a) REGEXP ".abc"');
            testParse(parser, { a: { $regex: /.ABC/, $options: 'i' } }, 'LOWER(a) REGEXP ".abc"');
         });

         it('should negate', () => {
            testParse(parser, { a: { $ne: /.ABC/i } }, 'LOWER(a) NOT REGEXP ".abc"');
            testParse(parser, { a: { $regex: /.ABC/, $negate: true } }, 'a NOT REGEXP ".ABC"');
            testParse(parser, { a: { $regex: /.ABC/i, $negate: true } }, 'LOWER(a) NOT REGEXP ".abc"');
         });

         it('should negate using $not', () => {
            testParse(parser, { a: { $not: /.ABC/i } }, 'NOT (LOWER(a) REGEXP ".abc")');
            testParse(parser, { a: { $not: /.ABC/ } }, 'NOT (a REGEXP ".ABC")');
            testParse(parser, { a: { $not: { $regex: /.ABC/i } } }, 'NOT (LOWER(a) REGEXP ".abc")');
         });

      });


      describe('$and', () => {

         it('should ignore if no key', () => {
            [
               {},
               [],
               [0], [0, 1],
               [''], ['foo'], ['foo', 'bar'],
               0, 1, 2,
               '', 'test'
            ].forEach(selector => testParse(parser, { $and: selector }, ''));
         });

         it('should and multiple conditions', () => {
            testParse(parser, { a: { $and: [ 1, 2, 3 ] } }, '(a = 1 AND a = 2 AND a = 3)');
         });

      });


      describe('$or', () => {

         it('should ignore if no key', () => {
            [
               {},
               [],
               [0], [0, 1],
               [''], ['foo'], ['foo', 'bar'],
               0, 1, 2,
               '', 'test'
            ].forEach(selector => testParse(parser, { $or: selector }, ''));
         });

         it('should or multiple conditions', () => {
            testParse(parser, { a: { $or: [ 1, 2, 3 ] } }, '(a = 1 OR a = 2 OR a = 3)');
         });

      });


      describe('$or', () => {

         it('should ignore if no key', () => {
            [
               {},
               [],
               [0], [0, 1],
               [''], ['foo'], ['foo', 'bar'],
               0, 1, 2,
               '', 'test'
            ].forEach(selector => testParse(parser, { $or: selector }, ''));
         });

         it('should exclude multiple conditions', () => {
            testParse(parser, { a: { $nor: [ { $eq: 1 }, 2, 3 ] } }, 'NOT (a = 1 OR a = 2 OR a = 3)');
         });

      });


      describe('$not', () => {

         it('should negate', () => {
            testParse(parser, { a: { $not: [ { $gt: 2 }, { $lt: 10 } ] } }, 'NOT (a > 2 AND a < 10)');
         })

      });


      // Functional

      describe('$negate', () => {

         it('should negate', () => {
            testParse(parser, { a: { $like: 'test', $negate: true } }, 'a NOT LIKE "test"');
         });

      });


      // Helpers

      describe('$cast', () => {

         it('should cast', () => {
            testParse(parser, { a: { $eq: 'foo', $cast: 'test' } }, 'a = CAST("foo" AS test)');
         });

      });

   
   });


   describe('Do use params', () => {

      const parser = new DefaultParser();
      

      // Logical

      describe('$eq / $ne', () => {

         it('should default to $q', () => {
            const d = new Date();

            testParse(parser, { a: 'Hello' }, 'a = $1', [ 'Hello' ]);
            testParse(parser, { a: 123 }, 'a = $1', [ 123 ]);
            testParse(parser, { a: null }, 'a IS $1', [ null ]);
            testParse(parser, { a: d }, 'a = $1', [ d ]);
            testParse(parser, { a: true }, 'a = $1', [ true ]);
            testParse(parser, { a: false }, 'a = $1', [ false ]);
            testParse(parser, { a: 'Hello', b: 123, c: true, d: null }, 'a = $1 AND b = $2 AND c = $3 AND d IS $4', [ 'Hello', 123, true, null ]);
            testParse(parser, { a: { b: { c: 'foo' }, d: 123 }}, 'a.b.c = $1 AND a.d = $2', [ 'foo', 123 ]);
         });

         it('should equal', () => {
            testParse(parser, { a: { $eq: 'Hello' } }, 'a = $1', [ 'Hello' ]);
            testParse(parser, { a: { $eq: 123 } }, 'a = $1', [ 123 ]);
         });

         it('should equal null', () => {
            [
               null, undefined
            ].forEach(value => testParse(parser, { a: { $eq: value } }, 'a IS $1', [ null ]));
         });

         it('should not equal', () => {
            testParse(parser, { a: { $ne: 'Hello' } }, 'a <> $1', [ 'Hello' ]);
            testParse(parser, { a: { $ne: 123 } }, 'a <> $1', [ 123 ]);
         });

         it('should not equal null', () => {
            [
               null, undefined
            ].forEach(value => testParse(parser, { a: { $ne: value } }, 'a IS NOT $1', [ null ]));
         });

      });


      describe('$gt / $gte', () => {

         it('should be greater than', () => {
            testParse(parser, { a: { $gt: 123 } }, 'a > $1', [ 123 ]);
         });

         it('should be greater than or equal', () => {
            testParse(parser, { a: { $gte: 123 } }, 'a >= $1', [ 123 ]);
         });

      });



      describe('$lt / $lte', () => {

         it('should be less than', () => {
            testParse(parser, { a: { $lt: 123 } }, 'a < $1', [ 123 ]);
         });

         it('should be less than or equal', () => {
            testParse(parser, { a: { $lte: 123 } }, 'a <= $1', [ 123 ]);
         });

      });


      describe('$in / $nin', () => {

         it('should be in', () => {
            testParse(parser, { a: { $in: [ 123 ] } }, 'a IN ($1)', [ 123 ]);
            testParse(parser, { a: { $in: [ 123, 456 ] } }, 'a IN ($1,$2)', [ 123, 456 ]);
         });

         it('should not be in', () => {
            testParse(parser, { a: { $nin: [ 123 ] } }, 'a NOT IN ($1)', [ 123 ]);
            testParse(parser, { a: { $nin: [ 123, 456 ] } }, 'a NOT IN ($1,$2)', [ 123, 456 ]);
         });

         it('should throw if not an array', () => {
            expect(() => testParse(parser, { a: { $in: 123 } })).toThrow();
            expect(() => testParse(parser, { a: { $nin: 123 } })).toThrow();
         });

      });


      describe('$regex', () => {

         it('should use default if using regular expression value', () => {
            testParse(parser, { a: /./ }, 'a REGEXP $1', [ '.' ]);
         });

         it('should use default if using regular expression value', () => {
            testParse(parser, { a: { $ne: /./ } }, 'a NOT REGEXP $1', [ '.' ]);
         });

         it('should use case insensitive', () => {
            testParse(parser, { a: /.ABC/i }, 'LOWER(a) REGEXP $1', [ '.abc' ]);
            testParse(parser, { a: { $regex: /.ABC/i } }, 'LOWER(a) REGEXP $1', [ '.abc' ]);
            testParse(parser, { a: { $regex: /.ABC/i, $options: 'i' } }, 'LOWER(a) REGEXP $1', [ '.abc' ]);
            testParse(parser, { a: { $regex: /.ABC/, $options: 'i' } }, 'LOWER(a) REGEXP $1', [ '.abc' ]);
         });

         it('should negate', () => {
            testParse(parser, { a: { $ne: /.ABC/i } }, 'LOWER(a) NOT REGEXP $1', [ '.abc' ]);
            testParse(parser, { a: { $regex: /.ABC/, $negate: true } }, 'a NOT REGEXP $1', [ '.ABC' ]);
            testParse(parser, { a: { $regex: /.ABC/i, $negate: true } }, 'LOWER(a) NOT REGEXP $1', [ '.abc' ]);
         });

         it('should negate using $not', () => {
            testParse(parser, { a: { $not: /.ABC/i } }, 'NOT (LOWER(a) REGEXP $1)', [ '.abc' ]);
            testParse(parser, { a: { $not: /.ABC/ } }, 'NOT (a REGEXP $1)', [ '.ABC' ]);
            testParse(parser, { a: { $not: { $regex: /.ABC/i } } }, 'NOT (LOWER(a) REGEXP $1)', [ '.abc' ]);
         });

      });


      describe('$and', () => {

         it('should be the default join operator', () => {
            testParse(parser, [ { a: true }, { b: true } ], 'a = $1 AND b = $2', [ true, true ]);
         });

         it('should and from array', () => {
            testParse(parser, { $and: [ { a: true }, { b: true } ] }, '(a = $1 AND b = $2)', [ true, true ]);
         });

         it('should ignore if no key', () => {
            [
               {},
               [],
               [0], [0, 1],
               [''], ['foo'], ['foo', 'bar'],
               0, 1, 2,
               '', 'test'
            ].forEach(selector => testParse(parser, { $and: selector }, ''));
         });

         it('should and multiple conditions', () => {
            testParse(parser, { a: { $and: [ 1, 2, 3 ] } }, '(a = $1 AND a = $2 AND a = $3)', [ 1, 2, 3 ]);
         });

      });


      describe('$or', () => {

         it('should or from array', () => {
            testParse(parser, { $or: [ { a: true }, { b: true } ] }, '(a = $1 OR b = $2)', [ true, true ]);
         });

         it('should ignore if no key', () => {
            [
               {},
               [],
               [0], [0, 1],
               [''], ['foo'], ['foo', 'bar'],
               0, 1, 2,
               '', 'test'
            ].forEach(selector => testParse(parser, { $or: selector }, '' ));
         });

         it('should or multiple conditions', () => {
            testParse(parser, { a: { $or: [ 1, 2, 3 ] } }, '(a = $1 OR a = $2 OR a = $3)', [ 1, 2, 3 ]);
         });

      });

      describe('$or', () => {

         it('should ignore if no key', () => {
            [
               {},
               [],
               [0], [0, 1],
               [''], ['foo'], ['foo', 'bar'],
               0, 1, 2,
               '', 'test'
            ].forEach(selector => testParse(parser, { $or: selector }, '' ));
         });

         it('should exclude multiple conditions', () => {
            testParse(parser, { a: { $nor: [ { $eq: 1 }, { $eq: 2 }, { $eq: 3 } ] } }, 'NOT (a = $1 OR a = $2 OR a = $3)', [ 1, 2, 3 ]);
         });

      });

      describe('$not', () => {

         it('should negate', () => {
            testParse(parser, { a: { $not: [ { $gt: 2 }, { $lt: 10 } ] } }, 'NOT (a > $1 AND a < $2)', [ 2, 10 ]);
         });

      });


      describe('Multiple conditions', () => {

         it('should combine two arrays', () => {
            testParse(parser, { a: { $or: [ { $in: [ 'a','b' ] }, { $in: [ 'y', 'z' ] } ] }, b: { $nin: [ 1, 2, 3 ] } }, '(a IN ($1,$2) OR a IN ($3,$4)) AND b NOT IN ($5,$6,$7)', [ 'a', 'b', 'y', 'z', 1, 2, 3 ]);
         });

         it('should process $and + $or', () => {
            testParse(parser, { a: { $or: [ { $eq: 2 } ], $and: [ { $eq: 10 } ] } }, "(a = 2) AND (a = 10)");
         });

      });

      // Functional
  
      // ....


      // Helpers

      describe('$cast', () => {

         it('should cast', () => {
            testParse(parser, { a: { $eq: 'foo', $cast: 'test' } }, 'a = CAST($1 AS test)', [ "foo" ]);
         });

      });

   });


   describe('Testing integration', () => {

      const parser = new DefaultParser();

      beforeAll(() => {
         Object.assign(parser.operators, {
            // override equality operator
            $between: (path, value, params, ctx, parser, helpers) => {
               if (!Array.isArray(value) || value.length !== 2) {
                 if (ctx.$to) {
                   value = [value, ctx.$to];
                 } else {
                   throw new Error('Between requires an array with exactly 2 elements or a $to attribute');
                 }
               }
         
               const [ col, low ] = processHelpers(helpers, parser.formatColumnName(path), parser.formatValue(value[0], params), params, ctx); 
               const [ , high ] = processHelpers(helpers, null, parser.formatValue(value[1], params), params, ctx); 
               
               return `(${col} BETWEEN ${low} AND ${high})`;
            }
         });
      });

      afterAll(() => {
         delete parser.operators;
      });

      it('should parse direct privitive as $eq operator', () => {
         expect( parser.parse({ a: 2 }) ).toBe('a = 2');
      });

      it('should ignore primitive if there is no $eq operator', () => {
         const $eq = parser.operators.$eq;
         parser.operators.$eq = null; // temp reset
         expect( parser.parse({ a: 2 }) ).toBe('');
         parser.operators.$eq = $eq; // restore
      });

      it('should ignore selector if there is no value to parse', () => {
         expect( parser.parse({ a: { $invalid: true } }) ).toBe('');
      });


      it('should parse direct privitive as $eq operator inside array (1 element)', () => {
         expect( parser.parse({ a: [ 2 ] }) ).toBe('a = 2');
      });

      it('should parse direct privitive as $in operator inside array (n elements)', () => {
         expect( parser.parse({ a: [ 2, 4, 6 ] }) ).toBe('a IN (2,4,6)');
      });

      it('should parse direct privitive as $or operator inside array (n elements)', () => {
         const $in = parser.operators.$in;
         parser.operators.$in = null; // temp reset
         expect( parser.parse({ a: [ 2, 4, 6 ] }) ).toBe('a = 2 OR a = 4 OR a = 6');
         parser.operators.$in = $in; // restore
      });

      it('should AND multiple fields', () => {
         expect( parser.parse({ a: 2, b: 4 }) ).toBe('a = 2 AND b = 4');
      });


      it('should return empty query if invalid selector', () => {
         const params = [];
         const selector = { $eq: true };
         const query = parser.parse(selector, params);

         expect( query ).toBe('');
         expect( params ).toEqual([]);
      });


      it('should parse custom operator BETWEEN', () => {
         const params = [];
         const selector = { foo: { $between: [ 10, 20 ], $cast: 'FLOAT' } };
         const query = parser.parse(selector, params);

         expect( query ).toBe('(foo BETWEEN CAST($1 AS FLOAT) AND CAST($2 AS FLOAT))');
         expect( params ).toEqual([ 10, 20 ]);
      });


      it('should intercept formatColumnName (1)', () => {
         const params = [];
         const selector = { foo: { $between: [ 10, 20 ], $cast: 'FLOAT' } };
         const formatColumnName = parser.__proto__.formatColumnName.bind(parser);

         parser.formatColumnName = path => {
            params.push('test');
            return 'FN(' + formatColumnName(path) + ',?)';
         };

         const query = parser.parse(selector, params);

         delete parser.formatColumnName;

         expect( query ).toBe('(FN(foo,?) BETWEEN CAST($2 AS FLOAT) AND CAST($3 AS FLOAT))');
         expect( params ).toEqual([ 'test', 10, 20 ]);
      });

      it('should intercept formatColumnName (2)', () => {
         const params = [];
         const selector = { foo: 1, bar: { $or: [ 4, 6 ] } };
         const formatColumnName = parser.__proto__.formatColumnName.bind(parser);

         parser.formatColumnName = path => {
            params.push('test');
            return 'FN(' + formatColumnName(path) + ',?)';
         };

         const query = parser.parse(selector, params);

         delete parser.formatColumnName;

         expect( query ).toBe('FN(foo,?) = $1 AND (FN(bar,?) = $3 OR FN(bar,?) = $5)');
         expect( params ).toEqual([ 'test', 1, 'test', 4, 'test', 6 ]);
      });

   });

});
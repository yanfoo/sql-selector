import parseSelector, {
   checkArray,
   isBoolean,
   isNull,
   isPrimitive,
   isRegExp,
   processHelpers
} from '../src/parser-utils';


describe('Testing parser-utils', () => {

   it('should test checkArray', () => {

      [
         [], [,,], [1,2]
      ].forEach(n => expect( checkArray(n) ).toBe(n));

      [
         null, undefined, 'abc', {}
      ].forEach(n => expect(() => checkArray(n) ).toThrow("Value must be an array"));

   });

   it('should test isBoolean', () => {

      [
         true, false
      ].forEach(n => expect( isBoolean(n) ).toBe(true));

      [
         -1, 0, 1, 'true', 'false'
      ].forEach(n => expect( isBoolean(n) ).toBe(false));
      
   });

   it('should test isNull', () => {

      // NOTE : empty arrays are null because they contain no data
      [
         null, undefined, []
      ].forEach(n => expect( isNull(n) ).toBe(true));

      [
         '', 0, false
      ].forEach(n => expect( isNull(n) ).toBe(false));

   });

   it('should test isPrimitive', () => {

      [
         null, -1, 0, 1, true, false, "test", ""
      ].forEach(n => expect( isPrimitive(n) ).toBe(true));

      [
         undefined, [], {}, () => {}
      ].forEach(n => expect( isPrimitive(n) ).toBe(false));
      
   });

   it('should test isRegExp', () => {

      [
         new RegExp("test"), new RegExp(), /./, /t/ig
      ].forEach(n => expect( isRegExp(n) ).toBe(true));

      [
         undefined, [], {}, () => {}, new Date(), ""
      ].forEach(n => expect( isRegExp(n) ).toBe(false));
      
   });

   it('should test processHelpers', () => {
      const params = [];
      const helpers = [
         (column, value, params, ctx) => (params.push(1), [`a${column}a`, value * 2 ]),
         (column, value, params, ctx) => (params.push(2), [`b${column}b`, value * 3 ]),
         (column, value, params, ctx) => (params.push(3), [`c${column}c`, value * 4 ]),
      ];
      const column = '_';
      const value = 2;
            
      expect( processHelpers(helpers, column, value, params) ).toEqual([ "abc_cba", 48 ]);
      expect( params ).toEqual([ 3, 2, 1 ]);
   });


   describe('Testing parser', () => {

      it("should parse", () => {
         const path = [];
         const selector = { foo: 123 };
         const params = [];
         const parser = {
            operators: {
               $eq: (path, value, params, ctx, parser, helpers) => {
                  params.push(value);

                  return `${path.join('')} = ?`;
               }
            }
         };
         const joinOper = ' TEST ';
         const helpers = [];

         const query = parseSelector(path, selector, params, parser, joinOper, helpers);

         expect( query ).toBe('foo = ?');
         expect( params ).toEqual([ 123 ]);
      });

      it('should fail for operator without column', () => {
         const path = [];
         const selector = { $eq: 123 };
         const params = [];
         const parser = {
            operators: {
               $eq: (path, value, params, ctx, parser, helpers) => {
                  params.push(value);

                  return null;
               }
            }
         };
         const joinOper = ' TEST ';
         const helpers = [];

         const query = parseSelector(path, selector, params, parser, joinOper, helpers);

         expect( query ).toBe('');
         expect( params ).toEqual([ 123 ]);  // still went through the operator
      });
          
   });


});
import parseSelector, { checkArray, isNull, isRegExp, processHelpers } from './parser-utils.js';


const defaultExpr = (path, op, value, params, ctx, parser, helpers) => {
   const newParams = params ? params.slice() : null;
   const paramsCount = params ? params.length : 0;
   const [ col, val ] = processHelpers(helpers, parser.formatColumnName(path), parser.formatValue(value, newParams), newParams, ctx);

   if (col) {
      // append params if column is valid
      if (newParams) {
         params.push(...newParams.slice(paramsCount));
      }
      return `${col}${op}${val}`;
   } else {
      return null;
   }
};

/**
 * Standard selectors : add new implements or override whenever ncessary
 * (Note: operator priority is determined by the selector.)
 */
const standardOperators = {
   $eq: (path, value, params, ctx, parser, helpers) => isRegExp(value) ? standardOperators.$regex(path, value, params, ctx, parser, helpers) : defaultExpr(path, isNull(value) ? ' IS ' : ' = ', value, params, ctx, parser, helpers),
   $ne: (path, value, params, ctx, parser, helpers) => isRegExp(value) ? standardOperators.$regex(path, value, params, { ...ctx, $negate: true }, parser, helpers) : defaultExpr(path, isNull(value) ? ' IS NOT ' : ' <> ', value, params, ctx, parser, helpers),
   $gt: (path, value, params, ctx, parser, helpers) => defaultExpr(path, ' > ', value, params, ctx, parser, helpers),
   $gte: (path, value, params, ctx, parser, helpers) => defaultExpr(path, ' >= ', value, params, ctx, parser, helpers),
   $lt: (path, value, params, ctx, parser, helpers) => defaultExpr(path, ' < ', value, params, ctx, parser, helpers),
   $lte: (path, value, params, ctx, parser, helpers) => defaultExpr(path, ' <= ', value, params, ctx, parser, helpers),

   $in: (path, value, params, ctx, parser, helpers) => defaultExpr(path, ' IN ', checkArray(value), params, ctx, parser, helpers),
   $nin: (path, value, params, ctx, parser, helpers) => defaultExpr(path, ' NOT IN ', checkArray(value), params, ctx, parser, helpers),

   $like: (path, value, params, ctx, parser, helpers) => defaultExpr(path, `${ctx && ctx.$negate ? ' NOT' : ''} LIKE `, value, params, ctx, parser, helpers),

   $regex: (path, value, params, ctx, parser, helpers) => {
      // NOTE : the $negate operator is not part of MongoDB's specifications, but it is a shortcut here. The official way to negate a regexp is to { $not: { $regxp: ... } }
      const ignoreCase = ((value instanceof RegExp) && value.ignoreCase) || (ctx && ctx.$options && (String(ctx.$options).toLowerCase().indexOf('i') >= 0));
      const negate = ctx && ctx.$negate;
      const op = negate ? ' NOT REGEXP ' : ' REGEXP ';

      if ((value instanceof RegExp) && ignoreCase) {
         let flags = value.flags;

         if (flags.indexOf('i') === -1) {
            flags = flags + 'i';
         }

         value = new RegExp(value.source.toLowerCase(), flags);
      }
      if (ignoreCase) {
         helpers.push((col, val) => [ `LOWER(${col})`, val ]);
      }

      return defaultExpr(path, op, value, params, ctx, parser, helpers);
   },

   $and: (path, value, params, _, parser, helpers) => {
      const parsed = parseSelector(path, value, params, parser, ' AND ', helpers);
      return parsed ? '(' + parsed + ')' : '';
   },
   $or: (path, value, params, _, parser, helpers) => {
      const parsed = parseSelector(path, value, params, parser, ' OR ', helpers);
      return parsed ? '(' + parsed + ')' : '';
   },

   $not: (path, value, params, _, parser, helpers) => 'NOT (' + parseSelector(path, [value], params, parser, ' AND ', helpers) + ')',
   $nor: (path, value, params, _, parser, helpers) => 'NOT (' + parseSelector(path, [value], params, parser, ' OR ', helpers) + ')',
};


export default standardOperators;
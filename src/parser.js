import parseSelector, { isBoolean, isNull, isRegExp } from './parser-utils.js';
import standardOperators from './operators.js';
import standardHelpers from './helpers.js';


class DefaultParser {

   /**
    * Copy standard operators to new instances
    */
   operators = { ...standardOperators };

   /**
    * Copy standard helpers to new instances
    */
   helpers = { ...standardHelpers };


   /**
   Return the placeholder token for the given param index
   @param {Number} index
   @return {String}
   */
   getPlaceholder(index) {
      return `$${index}`;
   }


   /**
   Parse the given selector and return a string.

   NOTE : it is recommended that params be specified!

   @param {Object} selector
   @param {Array} params      (optional) when specified, query will have placeholders and values will be ordered in that array
   @return {String}
   */
   parse(selector, params) {
      return parseSelector([], selector, params, this, ' AND ');
   }

   /**
   Format the column names so they are properly escaped

   @param {Array} path
   @return {String}
   */
   formatColumnName(path) {
      return path.join('.');
   }


   /**
   Format the column names so they are properly escaped

   @deprecated Please use formatColumnName instead
   @param {Array} path
   @return {String}
   */
   formatColumnNames(path) {
      console.warn("Deprecation warning! The method formatColumnNames was renamed to formatColumnName");
      return this.formatColumnName(path);
   }


   /**
   Format the given argument and return a primitive value that can be used in a SQL query

   @param {mixed} value
   @param {Array} params
   @param {mixed}
   */
   formatValue(value, params) {
      if (Array.isArray(value)) {
         return '(' + value.map(val => this.formatValue(val, params)).join(',') + ')';
      } else {
         if (isNull(value)) {
            if (params) {
               value = null;
            } else {
               value = 'NULL';
            }
         } else if (isBoolean(value)) {
            if (params) {
               value = !!value;
            } else {
               value = value.toString().toUpperCase();
            }
         } else if (isRegExp(value)) {
            if (params) {
               value = value.source;
            } else {
               value = JSON.stringify(value.source);
            }
         } else {
            if (!params) {
               value = JSON.stringify(value);
            }
         }

         if (params) {
            const index = params.length + 1;

            params.push(value);

            return this.getPlaceholder(index);
         } else {
            return value;
         }
      }
   }

}



export default DefaultParser;
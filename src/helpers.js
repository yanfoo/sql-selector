


const standardHelpers = {

   // https://sql.sh/fonctions/cast
   $cast: (column, value, type) => [ column, type ? `CAST(${value} AS ${type})` : value ],

};



export default standardHelpers;
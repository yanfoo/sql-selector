
/**
 * Check whether or not the argument is a SQL-NULL value
 * @param {Any} v
 * @return {Boolean}
 */
const isNull = v => (v === undefined) || (v === null) || (Array.isArray(v) && !v.length);

/**
 * Check whether or not the argument is a SQL-primitive value
 * @param {Any} v
 * @return {Boolean}
 */
const isPrimitive = v => (v === null) || (!isNaN(v) && !Array.isArray(v)) || (typeof v === 'string') || (v instanceof Date) || (v instanceof RegExp)

/**
 * Check whether or not the argument is a SQL-boolean value
 * @param {Any} v
 * @return {Boolean}
 */
const isBoolean = v => v === true || v === false;

/**
 * Check whether or not the argument is a regular expression
 * @param {Any} v
 * @return {Boolean}
 */
const isRegExp = v => v instanceof RegExp;

/**
 * Check whether or not the argument is a primitive array
 * @param {Any} v
 * @return {Boolean}
 */
const isPrimitiveArray = v => Array.isArray(v) ? v.every(e => isPrimitive(e)) : false;


/**
 * Check if the argument is an array and return it, otherwise throw an error
 * @throws {TypeError}
 * @param {Any} v
 * @return {Boolean}
 */
const checkArray = v => {
   if (Array.isArray(v)) {
      return v;
   } else {
      throw new TypeError('Value must be an array');
   }
};



/**
 * Unstack helpers for the given value
 * 
 * @param {String} column
 * @param {String} value
 * @param {Object} ctx
 * @param {Array} helpers
 * @return {String}
 */
const processHelpers = (helpers, column, value, params, ctx) => {
   for (let i = helpers.length - 1; i >= 0; --i) {
      [ column, value ] = helpers[i](column, value, params, ctx);
   }
   return [ column, value ];
};


/**
 * Parse the given selector starting with the given key path. If the selector is a primitive value,
 * an or if it is empty, an empty string will be returned.
 * 
 * @param {Array} path                the key path to the current selector (empty array for root)
 * @param {Object|Array} selector     the selector to parse
 * @param {Array} params              the parameter array to fill when parsing
 * @param {DefaultParser} parser      the current parser instance for this selector
 * @param {String} joinOper           the space-padded operand to use when joining selectors (e.g. ' AND ')
 * @param {Array} helpers             the helpers stack
 * @return {String}
 */
const parseSelector = (path, selector, params, parser, joinOper, helpers) => {
   if (!path.length && (!selector || isPrimitive(selector))) {
      return '';
   }

   const parts = [];
   const isArraySelector = Array.isArray(selector);

   if (isArraySelector && !path.length) {
      parts.push(...selector.map(part => parseSelector(path, part, params, parser, joinOper, helpers)));
   } else {
      const operators = [];
      const currentHelpers = helpers ? [ ...helpers ] : [];   // copy helpers so we preserve recursive states

      for (let key in selector) {
         const isFn = key && String(key).startsWith('$');
         const operator = isFn && parser.operators && parser.operators[key] ? parser.operators[key] : null;
         const helper = isFn && parser.helpers && parser.helpers[key] ? parser.helpers[key] : null;
         
         if (operator) {
            operators.push([ key, operator ]);
         } else if (helper) {
            const helperValue = selector[key];
            
            currentHelpers.push((column, value, params) => helper(column, value, helperValue, parser, params));
         }
      }

      if (operators.length) {
         for (const [ key, operator ] of operators) {
            const value = operator(path, selector[key], params, selector, parser, currentHelpers);

            if (value) {
               parts.push( value );
            }
         }
      } else {
         for (const key in selector) {
            const value = selector[key];
            const isMeta = key.startsWith('$');
            
            if (!isMeta && (!isArraySelector || path.length)) {
               const newPath = isArraySelector || !isNaN(key) ? path : path.concat([key]);

               if (isPrimitive(value)) {
                  if (parser?.operators?.$eq) {
                     parts.push( parser.operators.$eq(newPath, value, params, selector, parser, currentHelpers) );
                  }
               } else if (isPrimitiveArray(value) && (value.length > 1)) {
                  if (parser?.operators?.$in) {
                     parts.push( parser.operators.$in(newPath, value, params, selector, parser, currentHelpers) );
                  } else {
                     parts.push( parseSelector(newPath, value, params, parser, ' OR ', currentHelpers));   
                  }
               } else {
                  parts.push( parseSelector(newPath, value, params, parser, joinOper, currentHelpers));
               }
            }
         }
      }
   }

   return parts.filter(x => x).join(joinOper);
};


export default parseSelector;
export { checkArray, isBoolean, isNull, isPrimitive, isPrimitiveArray, isRegExp, processHelpers };
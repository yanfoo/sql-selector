import DefaultParser from '../parser.js';


/**
 * MySQL dialect
 */
class MySqlParser extends DefaultParser {

   /**
   Return the placeholder token
   @return {String}
   */
   getPlaceholder() {
      return '?'
   }

}



export default new MySqlParser();